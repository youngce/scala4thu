package class3.stackoverflow

/**
  * Created by mark on 05/03/2017.
  */
object Examples {
  def tailFac(n:Int,acc:Int):Int={
    if (n==0) acc
    else tailFac(n-1,n*acc)
  }
  def tailPow(x:Double,n:Int,acc:Double):Double={
    if (n>0) tailPow(x,n-1,x*acc)
    else if (n<0) tailPow(x,-n,acc)
    else acc

  }
  def tailFib(n:Int,prev:Int,next:Int):Int= {
    if (n==0) prev
    else if(n==1) next
    else tailFib(n-1,next,prev+next)
  }

}
