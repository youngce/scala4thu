package class3.recursion

/**
  * Created by mark on 05/03/2017.
  */
object Exercises {

  //以遞迴實現平方和函數1+4+...+n^2
  def sumOfSquares(n:Int):Int= if (n==1) 1 else n*n+sumOfSquares(n-1)
  //用遞迴求算0*1+1*2+2*3+3*4+…+(n-1)*n之和.
  def sum(n:Int):Int= if (n==1) 0 else n*(n-1)+sum(n-1)




}
