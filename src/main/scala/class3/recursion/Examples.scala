package class3.recursion

/**
  * Created by mark on 05/03/2017.
  */
object Examples {
  def factorial(n:Int):Int={
    if (n==0) 1 else n*factorial(n-1)
  }

  def fibonacci(n:Int):Int={
    if (n==0) 0
    else if(n<3) 1
    else fibonacci(n-1)+fibonacci(n-2)
  }
  def pow(x:Double,n:Int):Double={
    if (n>0) x*pow(x,n-1)
    else if (n<0) pow(x,-n)
    else 1
  }

}
