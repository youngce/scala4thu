package class3.closure

/**
  * Created by mark on 05/03/2017.
  */
object Examples {
  def factorial(n:Int):Int={
    def tailFac(x:Int,acc:Int):Int={
      if (x==0) acc
      else tailFac(x-1,n*acc)
    }
    tailFac(n,1)
  }

}
