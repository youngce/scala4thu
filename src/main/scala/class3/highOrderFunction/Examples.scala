package class3.highOrderFunction

/**
  * Created by mark on 05/03/2017.
  */
object Examples {
  def summation(n:Int,f:Int=>Int):Int=
    if (n==1) f(n) else f(n)+summation(n-1,f)


}
